def test_nginx_socket_listening(host):
    nginx = host.socket('tcp://0.0.0.0:80')
    assert nginx.is_listening

